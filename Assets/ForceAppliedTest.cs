﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceAppliedTest : MonoBehaviour {

	private int timer = 0;
	private bool mov = false;

	// Use this for initialization
	void Start () {
		//GetComponent<ConstantForce> ().enabled = true;
		mov = true;
	}

	// Update is called once per frame
	void Update () {
		timer++;

		if (timer >= 60)
		{
			//GetComponent<ConstantForce> ().enabled = false;
			mov = false;
		}

		if (mov)
			transform.position += new Vector3 (1, 0, 0);
	}
}
