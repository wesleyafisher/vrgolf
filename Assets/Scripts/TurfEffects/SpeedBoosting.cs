﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBoosting : MonoBehaviour {

    public GameObject ball;
    public float speed;
    public float max_speed;
    public bool upside_down;
    private bool check;

    private void FixedUpdate()
    {
        float mag = ball.GetComponent<Rigidbody>().velocity.magnitude;
        if (check && mag < max_speed)
        {
            if(upside_down)
                ball.GetComponent<Rigidbody>().AddForce(-speed, 0, 0, ForceMode.Impulse);
            else
                ball.GetComponent<Rigidbody>().AddForce(speed, 0, 0, ForceMode.Impulse);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Ball")
        {
            check = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Ball")
        {
            check = false;
        }
    }
}
