﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceZone : MonoBehaviour {

    public float bounce;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Ball")
        {
            Rigidbody rb = other.GetComponent<Rigidbody>();
            rb.AddForce(0, bounce, 0, ForceMode.Impulse);
        }
    }
}
