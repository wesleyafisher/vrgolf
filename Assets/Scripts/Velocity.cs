﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Velocity : MonoBehaviour {

    private Vector3 currentVelocity;
    private Vector3 previousPosition;
    private Vector3 currentPosition;

	// Use this for initialization
	void Start () {
        currentVelocity = previousPosition = currentPosition = new Vector3(0, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
        currentPosition = transform.position;
        currentVelocity = ((currentPosition - previousPosition) / Time.deltaTime);
        previousPosition = currentPosition;
	}

    public Vector3 getVelocity()
    {
        return currentVelocity;
    }
}
