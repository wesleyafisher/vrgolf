﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBallMove : MonoBehaviour {
    public GameObject ball;
    public float speed;
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("w"))
        {
            print("w was pressed");
            ball.GetComponent<Rigidbody>().AddForce(speed, 0, 0, ForceMode.Impulse);
        }
        if (Input.GetKeyDown("a"))
        {
            ball.GetComponent<Rigidbody>().AddForce(0, 0, speed, ForceMode.Impulse);
        }
        if (Input.GetKeyDown("s"))
        {
            ball.GetComponent<Rigidbody>().AddForce(-speed, 0, 0, ForceMode.Impulse);
        }
        if (Input.GetKeyDown("d"))
        {
            ball.GetComponent<Rigidbody>().AddForce(0, 0, -speed, ForceMode.Impulse);
        }
    }
}
