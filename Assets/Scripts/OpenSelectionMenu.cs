﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OpenSelectionMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //Replace with controller input
		if(Input.GetKeyDown("m"))
        {
            SceneManager.LoadScene("CourseSelection");
        }
	}
}
