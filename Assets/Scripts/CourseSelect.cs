﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CourseSelect : MonoBehaviour {
    public static int selectedCourse;
    public bool unlock_all;
    public Text errorText;

    public void courseOne()
    {
        selectedCourse = 1;
        SceneManager.LoadScene("Hole1");
    }

    public void courseTwo()
    {
        //Check previous hole's ball
        if (unlock_all || Ball01.courseComplete)
        {
            selectedCourse = 2;
            SceneManager.LoadScene("Hole2");
        }
        else
        {
            print("You must complete hole 1 before playing hole 2");
            errorText.text = "You must complete hole 1 before playing hole 2";
        }
    }

    public void courseThree()
    {
        //Check previous hole's ball
        if (unlock_all || Ball02.courseComplete)
        {
            selectedCourse = 3;
            SceneManager.LoadScene("Hole3");
        }
        else
        {
            print("You must complete hole 2 before playing hole 3");
            errorText.text = "You must complete hole 2 before playing hole 3";
        }
    }

    public void courseFour()
    {
        //Check previous hole's ball
        if (unlock_all || Ball03.courseComplete)
        {
            selectedCourse = 4;
            SceneManager.LoadScene("Hole4");
        }
        else
        {
            print("You must complete hole 3 before playing hole 4");
            errorText.text = "You must complete hole 3 before playing hole 4";
        }
    }

    public void courseFive()
    {
        //Check previous hole's ball
        if (unlock_all || Ball04.courseComplete)
        {
            selectedCourse = 5;
            SceneManager.LoadScene("Hole5");
        }
        else
        {
            print("You must complete hole 4 before playing hole 5");
            errorText.text = "You must complete hole 4 before playing hole 5";
        }
    }

    public void courseSix()
    {
        //Check previous hole's ball
        if (unlock_all || Ball05.courseComplete)
        {
            selectedCourse = 6;
            SceneManager.LoadScene("Hole6");
        }
        else
        {
            print("You must complete hole 5 before playing hole 6");
            errorText.text = "You must complete hole 5 before playing hole 6";
        }
    }

    public void courseSeven()
    {
        //Check previous hole's ball
        if (unlock_all || Ball06.courseComplete)
        {
            selectedCourse = 7;
            SceneManager.LoadScene("Hole7");
        }
        else
        {
            print("You must complete hole 6 before playing hole 7");
            errorText.text = "You must complete hole 6 before playing hole 7";
        }
    }

    public void courseEight()
    {
        //Check previous hole's ball
        if (unlock_all || Ball07.courseComplete)
        {
            selectedCourse = 8;
            SceneManager.LoadScene("Hole8");
        }
        else
        {
            print("You must complete hole 7 before playing hole 8");
            errorText.text = "You must complete hole 7 before playing hole 8";
        }
    }

    public void courseNine()
    {
        //Check previous hole's ball
        if (unlock_all || Ball08.courseComplete)
        {
            selectedCourse = 9;
            SceneManager.LoadScene("Hole9");
        }
        else
        {
            print("You must complete hole 8 before playing hole 9");
            errorText.text = "You must complete hole 8 before playing hole 9";
        }
    }

    public void cancel()
    {

        //Need something to return to the selectedCourse?
        switch(selectedCourse)
        {
            case 1:
                SceneManager.LoadScene("Hole1");
                break;
            case 2:
                SceneManager.LoadScene("Hole2");
                break;
            case 3:
                SceneManager.LoadScene("Hole3");
                break;
            case 4:
                SceneManager.LoadScene("Hole4");
                break;
            case 5:
                SceneManager.LoadScene("Hole5");
                break;
            case 6:
                SceneManager.LoadScene("Hole6");
                break;
            case 7:
                SceneManager.LoadScene("Hole7");
                break;
            case 8:
                SceneManager.LoadScene("Hole8");
                break;
            case 9:
                SceneManager.LoadScene("Hole9");
                break;
            default:
                SceneManager.LoadScene("Hole1");
                break;
        }
    }

}
