﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTeleporter1 : MonoBehaviour {

	public Transform ball;
	public Transform receiver;

	private bool ballIsOverlapping = false;
//	private Quaternion originalRotation = ball.rotation;

	
	// Update is called once per frame
	void Update ()
	{
		if (ballIsOverlapping)
		{
			Vector3 portalToBall = ball.position - transform.position;
			float dotProduct = Vector3.Dot (transform.up, portalToBall);

			// If true, ball has moved across portal
			if (dotProduct < 0f)
			{
				// Teleport the ball
				float rotationDiff = -Quaternion.Angle(transform.rotation, receiver.rotation);
				rotationDiff += 180;
				ball.RotateAround (ball.position, Vector3.up, rotationDiff);

				Vector3 positionOffset = Quaternion.Euler (0f, rotationDiff, 0f) * portalToBall;
				ball.position = receiver.position + positionOffset;

				ballIsOverlapping = false;
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Ball")
		{
			ballIsOverlapping = true;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.tag == "Ball")
		{
			ballIsOverlapping = false;
		}
	}
}
