﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftRightGravityShift : MonoBehaviour {
    public static float a = 10;
    public bool left;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Ball")
        {
            if(left)
                Physics.gravity = new Vector3(0, 0, a);
            else
                Physics.gravity = new Vector3(0, 0, -a);

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Ball")
        {
            Physics.gravity = new Vector3(0, -10, 0);
        }
    }

}
