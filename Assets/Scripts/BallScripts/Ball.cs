﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour {

//    public Text playerCamText;

	private float speedBoost = 1000f;
	private bool onBoostPad = false;
	private Vector3 normalMovementVector = Vector3.right;
	private Vector3 currentMovementVector;

//    void Start()
//    {
//        playerCamText.text = "";
//    }

    void OnTriggerEnter(Collider other)
    {
//        if (other.gameObject.CompareTag("Goal"))
//        {
//            playerCamText.text = "Goal!";
//        }
//		else 
		if(other.gameObject.CompareTag("SpeedBoost"))
		{
			onBoostPad = true;
			currentMovementVector += Vector3.forward * speedBoost; 
		}
    }

	void OnTriggerExit(Collider other)
	{
		if(other.gameObject.CompareTag("SpeedBoost"))
		{
			onBoostPad = false;
			speedBoost = 10f;
		}
	}

	void Update()
	{
		if (onBoostPad)
		{
			currentMovementVector += Vector3.forward * speedBoost;

			// Increase speed boost the longer ball is on boost pad
			speedBoost *= 2f;
		}
	}
}