﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball02 : MonoBehaviour
{
    // Physics Stuff
    private int timesHit;
    public float scale = 120f;
    private Rigidbody ballRB;
    public Velocity clubL;
    public Velocity clubR;

    // Other Stuff
    private Vector3 start;
    public Text introText;
    public Text completeText;
    public Button courseSelect;

    public static bool courseComplete;

    void Start()
    {
        timesHit = 0;
        start = transform.position;
        introText.text = "Hole 2 - Speed Highway\nOrange means \"Gotta Go Fast!\"";
        introText.gameObject.SetActive(true);
        completeText.gameObject.SetActive(false);
        courseSelect.gameObject.SetActive(true);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ClubL"))
        {
            //Debug.Log("HIT BALL\n");
            ballRB = gameObject.GetComponent<Rigidbody>();

            ballRB.AddForce(clubL.getVelocity() * scale);

            //Debug.Log("Force applied to ball: ");
            //Debug.Log((clubL.getVelocity() * scale).ToString());

            timesHit++;
        }

        if (other.gameObject.CompareTag("ClubR"))
        {
            //Debug.Log("HIT BALL\n");
            ballRB = gameObject.GetComponent<Rigidbody>();

            ballRB.AddForce(clubR.getVelocity() * scale);

            //Debug.Log("Force applied to ball: ");
            //Debug.Log((clubR.getVelocity() * scale).ToString());

            timesHit++;
        }

        if (other.gameObject.CompareTag("Goal"))
        {
            completeText.text = "Hole 2 Complete\nSwings: " + timesHit + "\n";
            switch (timesHit)
            {
                case 0:
                    completeText.text += "Hacker!";
                    break;
                case 1:
                    completeText.text += "Hole in One!";
                    break;
                case 2:
                    completeText.text += "Even Hole!";
                    break;
                case 3:
                    completeText.text += "Bogey!";
                    break;
                case 4:
                    completeText.text += "Double Bogey!";
                    break;
                default:
                    completeText.text += "Better luck next time!";
                    break;
            }
            introText.gameObject.SetActive(false);
            completeText.gameObject.SetActive(true);
            courseSelect.gameObject.SetActive(true);
            courseComplete = true;
        }

        if (other.gameObject.CompareTag("Reset"))
        {
            transform.position = start;
        }
    }
}