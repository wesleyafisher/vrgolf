﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvertGravity : MonoBehaviour {
    public static float a = 10;
	
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Ball")
        {
            Physics.gravity = new Vector3(0, a, 0);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Ball")
        {
            Physics.gravity = new Vector3(0, -9.8f, 0);
        }
    }

}
