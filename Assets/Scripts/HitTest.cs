﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitTest : MonoBehaviour {

	private Rigidbody clubRB, ballRB;
	private bool mov = false;
	private int timer = 0;

	public Transform ball;
	public float thrust;

	// Use this for initialization
	void Start () {
		clubRB = gameObject.GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetButtonDown ("Fire1"))
			mov = true;

		if (mov)
		{
			transform.position += new Vector3 (1, 0, 0);
			timer++;
		}

		if (timer >= 15)
			mov = false;
	}

	void OnCollisionEnter(Collision other)
	{
		if (other.collider.CompareTag ("Ball"))
		{
			Debug.Log ("HIT BALL\n");

			ballRB = other.collider.attachedRigidbody;
			ballRB.AddForce (clubRB.velocity);

			Debug.Log ("Force applied to ball: ");
			Debug.Log (clubRB.velocity.ToString());
		}
	}
		
}
