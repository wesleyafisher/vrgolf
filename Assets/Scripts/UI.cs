﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour {

    public Text introText;
    public Text completeText;
    public Button courseSelect;

    public void courseSelectClick()
    {
        introText.gameObject.SetActive(false);
        completeText.gameObject.SetActive(false);
        courseSelect.gameObject.SetActive(false);
        SceneManager.LoadScene("CourseSelection");
    }
}
