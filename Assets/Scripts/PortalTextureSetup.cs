﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTextureSetup : MonoBehaviour {

	public Camera portalCam1;
	public Camera portalCam2;

	public Material portalMat1;
	public Material portalMat2;

	// Use this for initialization
	void Start () {

		// If there is already a target texture on the camera, remove it
		if (portalCam2.targetTexture != null)
		{
			portalCam2.targetTexture.Release ();
		}
		portalCam2.targetTexture = new RenderTexture (Screen.width, Screen.height, 24);
		portalMat2.mainTexture = portalCam2.targetTexture;

		// If there is already a target texture on the camera, remove it
		if (portalCam1.targetTexture != null)
		{
			portalCam1.targetTexture.Release ();
		}
		portalCam1.targetTexture = new RenderTexture (Screen.width, Screen.height, 24);
		portalMat1.mainTexture = portalCam1.targetTexture;
	}

}
