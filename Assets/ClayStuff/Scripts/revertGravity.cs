﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class revertGravity : MonoBehaviour {

	private static double g = -9.81;
	private float a = (float)g;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.name == "Player") {
			Physics.gravity = new Vector3 (0, a, 0);
		}
	}
}
