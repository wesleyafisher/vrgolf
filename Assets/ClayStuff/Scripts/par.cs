﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class par : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<Text> ().text = "";
	}
	
	// Update is called once per frame
	void Update () {
		if (flow.actual == 0) {

		}

		if (flow.actual == 1) {
			// hole in one
			GetComponent<Text> ().text = "Hole in one!";
		}

		if (flow.actual == 2) {
			// even hole
			GetComponent<Text> ().text = "Even hole!";
		}

		if (flow.actual == 3) {
			// bogey
			GetComponent<Text> ().text = "Bogey!";
		}

		if (flow.actual == 4) {
			// double bogey
			GetComponent<Text> ().text = "Double Bogey!";
		}

		if (flow.actual > 4) {
			// step it up
			GetComponent<Text> ().text = "Maybe try another game!";
		}

		flow.actual = 0;
	}
}
