﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class invertedGravity : MonoBehaviour {

	public int grav = 2;

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody> ().useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.name == "Player") {
			Physics.gravity = new Vector3 (0, grav, 0);
		}
	}
}
