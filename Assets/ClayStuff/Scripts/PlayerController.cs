﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public Transform arrow;
	public float f = 100;

    void Start()
    {
      
    }

	void Update(){
		if(Input.GetButtonDown ("Fire1")){
			flow.currStrokes++;
			flow.strokesTotal++;
			GetComponent<Rigidbody> ().AddRelativeForce (0,0,f);
		}

		if(Input.GetKeyDown("space")){
			GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);
			GetComponent<Transform>().eulerAngles = new Vector3(0,0,0);
			arrow.GetComponent<Transform> ().position = transform.position;
		}

		if(Input.GetKey("a"))
		{
			transform.Rotate (0,-1,0);
		}

		if(Input.GetKey("d"))
		{
			transform.Rotate (0,1,0);
		}

		if(Input.GetKey("w"))
		{
			f += 5;
		}

		if(Input.GetKey("s"))
		{
			f -= 5;
		}
			
	}

    void OnTriggerEnter(Collider other)
    {

		if (other.name == "Hole") {
			flow.actual = flow.currStrokes;
			flow.currStrokes = 0;
			GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);
		}
    }
}