﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gravity : MonoBehaviour {

	public int grav = -2;
	public int bounce = 5;

	void Awake(){
		Physics.gravity = new Vector3 (0, grav, 0);
		Physics.bounceThreshold = bounce;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
